package ru.kapodes.tictactoe.gamelogic.session;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.game.Game;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.time.Instant.now;
import static java.util.stream.Collectors.toList;
import static ru.kapodes.tictactoe.gamelogic.session.GameSession.create;

@Service
@RequiredArgsConstructor
@Slf4j
public class SessionsService {

    private final Map<UUID, GameSession> games = new HashMap<>();

    public synchronized UUID startNew(Game game, Player one, Player two) {
        GameSession gameSession = create(game, one, two);
        games.put(gameSession.getUuid(), gameSession);
        return gameSession.getUuid();
    }

    public synchronized Collection<UUID> activeSessionsForPlayer(Player player) {
        return games.values().stream()
                .filter(gameSession -> gameSession.isForPlayer(player))
                .map(GameSession::getUuid)
                .collect(toList());
    }

    public synchronized GameSession session(UUID gameId, Player player) {
        GameSession session = games.get(gameId);
        if (session == null) throw new BadRequestException("Game session not found");
        if (!session.isForPlayer(player)) throw new BadRequestException("Access forbidden");
        return session;
    }

    synchronized void removeOld() {
        Instant cutOffTime = now().minus(1, ChronoUnit.MINUTES);
        Iterator<Map.Entry<UUID, GameSession>> iterator = games.entrySet().iterator();
        while (iterator.hasNext()) {
            GameSession session = iterator.next().getValue();
            if (session.finishedAt().map(ft -> ft.isBefore(cutOffTime)).orElse(false)) {
                iterator.remove();
                log.debug("Removed old session {}", session.getUuid());
            }
        }
    }
}
