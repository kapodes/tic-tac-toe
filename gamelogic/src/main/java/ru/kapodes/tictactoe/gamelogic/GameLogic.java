package ru.kapodes.tictactoe.gamelogic;

import lombok.RequiredArgsConstructor;
import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.game.Game;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.game.GameValidator;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.player.PlayersService;
import ru.kapodes.tictactoe.gamelogic.session.GameSession;
import ru.kapodes.tictactoe.gamelogic.session.SessionsService;
import ru.kapodes.tictactoe.gamelogic.game.strategy.ComputerStrategy;
import ru.kapodes.tictactoe.gamelogic.stats.LeaderBoards;
import ru.kapodes.tictactoe.gamelogic.stats.TimeService;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static ru.kapodes.tictactoe.gamelogic.game.Game.newGame;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.PENDING;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.WON;

@Service
@RequiredArgsConstructor
public class GameLogic {

    private final PlayersService playersService;
    private final SessionsService sessionsService;
    private final ComputerStrategy computerStrategy;
    private final GameValidator validator;
    private final TimeService timeService;
    private final LeaderBoards stats;

    public UUID createNewWithComputer(Player player) {
        Game game = Game.newGame(player, playersService.computer());
        UUID gameId = sessionsService.startNew(game, player, playersService.computer());
        timeService.startPlayerTurn(gameId, player);
        return gameId;
    }

    public GameResult makeComputerTurn(UUID gameId) {
        GameSession session = sessionsService.session(gameId, playersService.computer());
        MatrixPosition move = computerStrategy.nextMove(session.getGame());
        return makePlayerTurn(gameId, playersService.computer(), move.row, move.column);
    }

    public GameResult makePlayerTurn(UUID gameId, Player player, int row, int column) {
        GameResult result;
        GameSession session = sessionsService.session(gameId, player);
        synchronized (session) {
            if (session.isFinished()) throw new BadRequestException("Game finished");
            MatrixPosition movePosition = session.getGame().makeAMove(player, row, column);
            result = validator.validate(session.getGame(), movePosition);
            session.setGameResult(result);
        }
        finishTurn(gameId, player, session);
        return result;
    }

    private void finishTurn(UUID gameId, Player player, GameSession session) {
        timeService.endPlayerTurn(gameId, player);
        if (session.getGameResult() == GameResult.WON) {
            stats.collectStats(player, session, session.getGame().turnsCount(player));
        } else if (session.getGameResult() == GameResult.PENDING) {
            timeService.startPlayerTurn(gameId, session.otherPlayer(player));
        }
    }
}
