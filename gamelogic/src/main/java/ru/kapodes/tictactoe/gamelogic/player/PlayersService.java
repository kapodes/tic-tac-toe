package ru.kapodes.tictactoe.gamelogic.player;

import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

@Service
public class PlayersService {

    private final Map<String, Player> activePlayers = new ConcurrentHashMap<>();
    private final Player COMPUTER = addPlayer("computer");

    public Player addPlayer(String name) {
        if (activePlayers.containsKey(name)) throw new BadRequestException("Player %s already present", name);
        Player player = new Player(name);
        activePlayers.put(name, player);
        return player;
    }

    public Player getPlayer(String name) {
        return findPlayer(name).orElseThrow(() -> new NotFoundException("Not found player with name %s", name));
    }

    public Optional<Player> findPlayer(String name) {
        return Optional.ofNullable(activePlayers.get(name));
    }

    public Player computer() {
        return COMPUTER;
    }

    public Collection<Player> getPlayers() {
        return activePlayers.values();
    }

    //can add update, remove, password, etc
}
