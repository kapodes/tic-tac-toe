package ru.kapodes.tictactoe.gamelogic.game;

import ru.kapodes.tictactoe.gamelogic.matrix.Matrix;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixDirection;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.DRAW;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.PENDING;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.WON;

@Service
public class GameValidator {

    public GameResult validate(Game game, MatrixPosition lastMove) {
        if (hasWon(game, lastMove)) return WON;
        if (noMovesLeft(game)) return DRAW;
        return PENDING;
    }

    private boolean hasWon(Game game, MatrixPosition lastMove) {
        Matrix<Character> board = game.board();
        return generateVariants(board, lastMove)
                .anyMatch(line -> lineOfSame(line, board));
    }

    private Stream<List<MatrixPosition>> generateVariants(Matrix<Character> board, MatrixPosition lastMove) {
        Stream<List<MatrixPosition>> lines = Stream.of(MatrixDirection.values())
                .map(dir -> generateLine(lastMove, dir, board))
                .filter(line -> line.size() == 3);
        Stream<List<MatrixPosition>> crosses = Stream.of(MatrixDirection.values())
                .map(dir -> generateCross(lastMove, dir, board))
                .filter(line -> line.size() == 3);
        return Stream.concat(lines, crosses);
    }

    private List<MatrixPosition> generateLine(MatrixPosition start, MatrixDirection direction, Matrix matrix) {
        MatrixPosition second = start.advance(direction);
        MatrixPosition third = second.advance(direction);
        return Stream.of(start, second, third).filter(matrix::isInBounds).collect(toList());
    }

    private List<MatrixPosition> generateCross(MatrixPosition center, MatrixDirection direction, Matrix matrix) {
        MatrixPosition second = center.advance(direction);
        MatrixPosition third = center.advance(direction.opposite());
        return Stream.of(center, second, third).filter(matrix::isInBounds).collect(toList());
    }

    private <T> boolean lineOfSame(List<MatrixPosition> line, Matrix<T> matrix) {
        T first = matrix.getAtPosition(line.get(0));
        return line.stream().map(matrix::getAtPosition).allMatch(each -> each.equals(first));
    }

    private boolean noMovesLeft(Game game) {
        return !game.board().anyMatch(BoardCharacters.EMPTY);
    }
}
