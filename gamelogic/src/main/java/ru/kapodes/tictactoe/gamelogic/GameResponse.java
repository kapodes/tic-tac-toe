package ru.kapodes.tictactoe.gamelogic;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.matrix.Matrix;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.session.GameSession;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static ru.kapodes.tictactoe.gamelogic.game.BoardCharacters.EMPTY;

@Getter
@RequiredArgsConstructor
class GameResponse {
    private final Player nextTurnPlayer;
    private final GameResult result;
    private final List<MarkedPosition> markedPositions;

    static GameResponse toResponse(GameSession session) {
        Matrix<Character> board = session.getGame().board();
        List<MarkedPosition> markedPositions = session.getGame()
                .positionsFor(ch -> !EMPTY.equals(ch))
                .stream()
                .map(pos -> new MarkedPosition(pos.row, pos.column, board.getAtPosition(pos)))
                .collect(toList());

        return new GameResponse(session.getGame().nextTurnPlayer(), session.getGameResult(), markedPositions);
    }

    @RequiredArgsConstructor
    @Getter
    static class MarkedPosition {
        private final int row;
        private final int column;
        private final char mark;
    }
}
