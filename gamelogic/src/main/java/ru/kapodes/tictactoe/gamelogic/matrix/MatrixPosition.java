package ru.kapodes.tictactoe.gamelogic.matrix;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MatrixPosition {

    public final int row;
    public final int column;

    public static MatrixPosition positionOf(int row, int column) {
        return new MatrixPosition(row, column);
    }

    public MatrixPosition advance(MatrixDirection direction) {
        switch (direction) {
            case UP:
                return new MatrixPosition(row - 1, column);
            case DOWN:
                return new MatrixPosition(row + 1, column);
            case RIGHT:
                return new MatrixPosition(row, column + 1);
            case LEFT:
                return new MatrixPosition(row, column - 1);
            case UP_RIGHT:
                return new MatrixPosition(row - 1, column + 1);
            case UP_LEFT:
                return new MatrixPosition(row - 1, column - 1);
            case DOWN_RIGHT:
                return new MatrixPosition(row + 1, column + 1);
            case DOWN_LEFT:
                return new MatrixPosition(row + 1, column - 1);
            default:
                throw new UnsupportedOperationException("Unsupported direction: " + direction);
        }
    }
}
