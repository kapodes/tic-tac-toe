package ru.kapodes.tictactoe.gamelogic.game;

public enum GameResult {
    PENDING, WON, DRAW
}
