package ru.kapodes.tictactoe.gamelogic.matrix;

public enum MatrixDirection {
    UP {
        @Override
        public MatrixDirection opposite() {
            return DOWN;
        }
    }, RIGHT {
        @Override
        public MatrixDirection opposite() {
            return LEFT;
        }
    }, DOWN {
        @Override
        public MatrixDirection opposite() {
            return UP;
        }
    }, LEFT {
        @Override
        public MatrixDirection opposite() {
            return RIGHT;
        }
    },
    UP_RIGHT {
        @Override
        public MatrixDirection opposite() {
            return DOWN_LEFT;
        }
    }, UP_LEFT {
        @Override
        public MatrixDirection opposite() {
            return DOWN_RIGHT;
        }
    },
    DOWN_RIGHT {
        @Override
        public MatrixDirection opposite() {
            return UP_LEFT;
        }
    }, DOWN_LEFT {
        @Override
        public MatrixDirection opposite() {
            return UP_RIGHT;
        }
    };

    public abstract MatrixDirection opposite();
}
