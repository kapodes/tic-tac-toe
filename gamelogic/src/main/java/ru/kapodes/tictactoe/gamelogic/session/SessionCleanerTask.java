package ru.kapodes.tictactoe.gamelogic.session;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SessionCleanerTask {
    private final SessionsService sessionsService;

    @Scheduled(fixedDelay = 30 * 1_000)
    public void removeOldSessions() {
        log.debug("Running session clean task...");
        sessionsService.removeOld();
        log.debug("Stale sessions removed.");
    }
}
