package ru.kapodes.tictactoe.gamelogic.game.strategy;

import ru.kapodes.tictactoe.gamelogic.game.Game;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;

public interface ComputerStrategy {

    MatrixPosition nextMove(Game game);
}
