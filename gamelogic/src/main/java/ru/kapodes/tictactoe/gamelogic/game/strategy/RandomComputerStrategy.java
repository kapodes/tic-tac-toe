package ru.kapodes.tictactoe.gamelogic.game.strategy;

import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.game.Game;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Collections.shuffle;
import static ru.kapodes.tictactoe.gamelogic.game.BoardCharacters.EMPTY;

@Service
public class RandomComputerStrategy implements ComputerStrategy {

    @Override
    public MatrixPosition nextMove(Game game) {
        List<MatrixPosition> possiblePositions = game.positionsFor(EMPTY::equals);
        if (possiblePositions.isEmpty()) throw new BadRequestException("Board is full");

        shuffle(possiblePositions);
        return possiblePositions.get(0);
    }
}
