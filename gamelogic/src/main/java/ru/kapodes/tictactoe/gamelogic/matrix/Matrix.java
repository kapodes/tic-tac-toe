package ru.kapodes.tictactoe.gamelogic.matrix;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Optional.empty;
import static ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition.positionOf;

public class Matrix<T> {
    private final T[][] matrix;
    private final Class<T> clazz;

    @SuppressWarnings("unchecked")
    public Matrix(Class<T> clazz, int size) {
        matrix = (T[][]) Array.newInstance(clazz, size, size);
        this.clazz = clazz;
    }

    private Matrix(Class<T> clazz, T[][] matrix) {
        this.clazz = clazz;
        this.matrix = matrix;
    }

    public void setAll(T value) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                matrix[row][col] = value;
            }
        }
    }

    public boolean anyMatch(T value) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                if (matrix[row][col].equals(value)) return true;
            }
        }
        return false;
    }

    public List<MatrixPosition> matchingPositions(Predicate<T> predicate) {
        List<MatrixPosition> positions = new ArrayList<>();
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                MatrixPosition position = positionOf(row, col);
                if (predicate.test(getAtPosition(position))) positions.add(position);
            }
        }
        return positions;
    }

    @SuppressWarnings("unchecked")
    public Matrix<T> copy() {
        T[][] copy = (T[][]) Array.newInstance(clazz, matrix.length, matrix[0].length);
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                copy[row][col] = matrix[row][col];
            }
        }
        return new Matrix<>(clazz, copy);
    }

    public Optional<MatrixPosition> tryAdvance(MatrixPosition current, MatrixDirection direction) {
        MatrixPosition advanced = current.advance(direction);
        return isInBounds(advanced) ? Optional.of(advanced) : empty();
    }

    public boolean isInBounds(MatrixPosition position) {
        if (position.row < 0 || position.row > matrix.length - 1) return false;
        if (position.column < 0 || position.column > matrix[position.row].length - 1) return false;
        return true;
    }

    public T getAtPosition(MatrixPosition position) {
        return matrix[position.row][position.column];
    }

    public void setAtPosition(MatrixPosition position, T newValue) {
        matrix[position.row][position.column] = newValue;
    }
}
