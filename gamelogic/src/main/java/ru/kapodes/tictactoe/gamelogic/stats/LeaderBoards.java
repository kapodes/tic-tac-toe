package ru.kapodes.tictactoe.gamelogic.stats;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.stats.TimeService;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.session.GameSession;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.Comparator.comparing;

@Service
@RequiredArgsConstructor
public class LeaderBoards {
    private final TimeService timeStats;
    private final Set<PlayerStat> leaderBoard = new TreeSet<>(PlayerStat.comparator());

    public void collectStats(Player player, GameSession session, int turns) {
        long timeSpent = timeStats.getAndRemoveStat(session.getUuid(), player);
        synchronized (this) {
            leaderBoard.add(new PlayerStat(player.getName(), turns, timeSpent));
        }
    }

    public synchronized List<PlayerStat> getLeaders() {
        return new ArrayList<>(leaderBoard);
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    public static class PlayerStat {
        public final String name;
        public final int turnsCount;
        public final long timeSpent;

        public static Comparator<PlayerStat> comparator() {
            return comparing(PlayerStat::getTurnsCount)
                    .thenComparing(PlayerStat::getTimeSpent)
                    .thenComparing(PlayerStat::getName);
        }
    }
}
