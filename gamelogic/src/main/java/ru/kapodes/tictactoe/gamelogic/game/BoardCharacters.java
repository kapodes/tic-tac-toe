package ru.kapodes.tictactoe.gamelogic.game;

public class BoardCharacters {
    public static final Character X = 'X';
    public static final Character O = 'O';
    public static final Character EMPTY = ' ';
}
