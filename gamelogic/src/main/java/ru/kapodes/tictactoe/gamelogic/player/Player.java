package ru.kapodes.tictactoe.gamelogic.player;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class Player {
    private final String name;
}
