package ru.kapodes.tictactoe.gamelogic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    public NotFoundException(String format, Object... args) {
        super(String.format(format, args));
    }
}
