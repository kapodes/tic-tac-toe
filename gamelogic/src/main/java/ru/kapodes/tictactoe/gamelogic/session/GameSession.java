package ru.kapodes.tictactoe.gamelogic.session;

import lombok.*;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.game.Game;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static java.time.Instant.now;
import static java.util.UUID.randomUUID;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.DRAW;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.PENDING;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.WON;

@EqualsAndHashCode(of = "uuid")
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class GameSession {
    private final UUID uuid = randomUUID();
    private final Instant startedAt = now();
    private final Game game;
    private final Player one;
    private final Player two;
    private GameResult gameResult = GameResult.PENDING;
    private Instant finishedAt;

    public static GameSession create(@NonNull Game game, @NonNull Player one, @NonNull Player two) {
        return new GameSession(game, one, two);
    }

    public boolean isForPlayer(Player player) {
        return one.equals(player) || two.equals(player);
    }

    public Player otherPlayer(Player player) {
        return one.equals(player) ? two : one;
    }

    public void setGameResult(GameResult gameResult) {
        this.gameResult = gameResult;
        if (isFinished()) finishedAt = now();
    }

    public boolean isFinished() {
        return gameResult == GameResult.WON || gameResult == GameResult.DRAW;
    }

    public Optional<Instant> finishedAt() {
        return Optional.ofNullable(finishedAt);
    }

}
