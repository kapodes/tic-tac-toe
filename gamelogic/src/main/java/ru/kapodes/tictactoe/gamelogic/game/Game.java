package ru.kapodes.tictactoe.gamelogic.game;

import lombok.RequiredArgsConstructor;
import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.matrix.Matrix;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;
import ru.kapodes.tictactoe.gamelogic.player.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static ru.kapodes.tictactoe.gamelogic.game.BoardCharacters.O;
import static ru.kapodes.tictactoe.gamelogic.game.BoardCharacters.X;
import static ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition.positionOf;

public class Game {
    private final Matrix<Character> board;
    private final TTTPlayer playerX;
    private final TTTPlayer playerO;
    private TTTPlayer nextPlayer;
    private final Map<Player, Integer> turnCount = new HashMap<>();

    private Game(Matrix<Character> board, Player x, Player o) {
        this.board = board;
        playerX = new TTTPlayer(x, X);
        playerO = new TTTPlayer(o, O);
        nextPlayer = playerX;
        turnCount.put(x, 0);
        turnCount.put(o, 0);
    }

    public static Game newGame(Player x, Player o) {
        Matrix<Character> gameMatrix = new Matrix<>(Character.class, 3);
        gameMatrix.setAll(BoardCharacters.EMPTY);
        return new Game(gameMatrix, x, o);
    }

    public Player nextTurnPlayer() {
        return nextPlayer.player;
    }

    public Matrix<Character> board() {
        return board.copy();
    }

    public List<MatrixPosition> positionsFor(Predicate<Character> condition) {
        return board.matchingPositions(condition);
    }

    public int turnsCount(Player player) {
        return turnCount.getOrDefault(player, 0);
    }

    public MatrixPosition makeAMove(Player player, int row, int column) {
        checkNextPlayer(player);

        MatrixPosition position = positionOf(row, column);
        checkPosition(position);

        makeAMove(nextPlayer, position);
        switchNextPlayer(player);
        addTurn(player);
        return position;
    }

    private void checkNextPlayer(Player player) {
        if (!nextPlayer.player.equals(player)) throw new BadRequestException("Wrong player");
    }

    private void checkPosition(MatrixPosition position) {
        if (!board.isInBounds(position)) throw new BadRequestException("Illegal move position");
        if (!board.getAtPosition(position).equals(BoardCharacters.EMPTY))
            throw new BadRequestException("Position not empty");
    }

    private void makeAMove(TTTPlayer player, MatrixPosition position) {
        board.setAtPosition(position, player.playerChar);
    }

    private void switchNextPlayer(Player current) {
        nextPlayer = playerX.player.equals(current) ? playerO : playerX;
    }

    private void addTurn(Player player) {
        turnCount.compute(player, (p, old) -> ++old);
    }

    @RequiredArgsConstructor
    private class TTTPlayer {
        private final Player player;
        private final Character playerChar;
    }
}

