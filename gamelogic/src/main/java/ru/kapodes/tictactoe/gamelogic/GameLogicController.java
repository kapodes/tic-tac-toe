package ru.kapodes.tictactoe.gamelogic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.player.PlayersService;
import ru.kapodes.tictactoe.gamelogic.session.GameSession;
import ru.kapodes.tictactoe.gamelogic.session.SessionsService;
import ru.kapodes.tictactoe.gamelogic.stats.LeaderBoards;
import ru.kapodes.tictactoe.gamelogic.stats.LeaderBoards.PlayerStat;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static ru.kapodes.tictactoe.gamelogic.GameResponse.toResponse;

@RestController("gamelogic")
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/gamelogic")
@CrossOrigin(origins = "http://localhost:8080")
public class GameLogicController {
    private final GameLogic logic;
    private final SessionsService sessions;
    private final PlayersService playersService;
    private final LeaderBoards leaderBoards;

    @PostMapping("/players/{name}")
    public Player createPlayer(@PathVariable String name) {
        log.info("Got request for new player: {}", name);
        return playersService.addPlayer(name);
    }

    @GetMapping("/players")
    public Collection<Player> players() {
        return playersService.getPlayers();
    }

    @PostMapping("/game/computer")
    public UUID createGame(@RequestParam String name) {
        Player player = playersService.getPlayer(name);
        return logic.createNewWithComputer(player);
    }

    @GetMapping("/game/{gameId}")
    public GameResponse getGame(@PathVariable UUID gameId, @RequestParam String name) {
        Player player = playersService.getPlayer(name);
        GameSession session = sessions.session(gameId, player);
        return toResponse(session);
    }

    @PostMapping("/game/{gameId}/playerTurn/{name}")
    public GameResult playerTurn(@PathVariable UUID gameId, @PathVariable String name, @RequestParam int row, @RequestParam int column) {
        Player player = playersService.getPlayer(name);
        log.info("Got request for player turn: {} r={} c={}", name, row, column);
        return logic.makePlayerTurn(gameId, player, row, column);
    }

    @PostMapping("/game/{gameId}/computerTurn")
    public GameResult computerTurn(@PathVariable UUID gameId) {
        return logic.makeComputerTurn(gameId);
    }

    @GetMapping("/leaders")
    public List<PlayerStat> leaders() {
        return leaderBoards.getLeaders();
    }

    @GetMapping("/sessions/{name}")
    public Collection<UUID> sessions(@PathVariable String name) {
        Player player = playersService.getPlayer(name);
        return sessions.activeSessionsForPlayer(player);
    }
}
