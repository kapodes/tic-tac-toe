package ru.kapodes.tictactoe.gamelogic.stats;

import com.google.common.base.Stopwatch;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kapodes.tictactoe.gamelogic.player.Player;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Stopwatch.createStarted;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Service
public class TimeService {

    private final Map<PlayerGame, Stopwatch> timers = new ConcurrentHashMap<>();
    private final Map<PlayerGame, Long> accumulators = new ConcurrentHashMap<>();

    public void startPlayerTurn(UUID gameId, Player player) {
        timers.put(new PlayerGame(gameId, player), createStarted());
    }

    public void endPlayerTurn(UUID gameId, Player player) {
        PlayerGame pg = new PlayerGame(gameId, player);
        Stopwatch stopwatch = timers.remove(pg);
        if (stopwatch != null) {
            accumulators.compute(pg, (key, oldVal) -> add(oldVal, stopwatch));
        }
    }

    private static Long add(Long accumulator, Stopwatch stopwatch) {
        return stopwatch.elapsed(MILLISECONDS) + (accumulator != null ? accumulator : 0L);
    }

    public long getAndRemoveStat(UUID uuid, Player player) {
        Long stat = accumulators.remove(new PlayerGame(uuid, player));
        return stat != null ? stat / 1000 : 0L; //hey Elvis
    }

    @RequiredArgsConstructor
    @EqualsAndHashCode
    private static class PlayerGame {
        private final UUID gameId;
        private final Player player;
    }
}
