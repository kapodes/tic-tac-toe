package ru.kapodes.tictactoe.gamelogic;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.kapodes.tictactoe.gamelogic.exceptions.BadRequestException;
import ru.kapodes.tictactoe.gamelogic.game.Game;
import ru.kapodes.tictactoe.gamelogic.game.GameResult;
import ru.kapodes.tictactoe.gamelogic.game.GameValidator;
import ru.kapodes.tictactoe.gamelogic.game.strategy.ComputerStrategy;
import ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition;
import ru.kapodes.tictactoe.gamelogic.player.Player;
import ru.kapodes.tictactoe.gamelogic.player.PlayersService;
import ru.kapodes.tictactoe.gamelogic.session.GameSession;
import ru.kapodes.tictactoe.gamelogic.session.SessionsService;
import ru.kapodes.tictactoe.gamelogic.stats.LeaderBoards;
import ru.kapodes.tictactoe.gamelogic.stats.TimeService;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static java.util.UUID.randomUUID;
import static ru.kapodes.tictactoe.gamelogic.game.Game.newGame;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.DRAW;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.PENDING;
import static ru.kapodes.tictactoe.gamelogic.game.GameResult.WON;
import static ru.kapodes.tictactoe.gamelogic.matrix.MatrixPosition.positionOf;
import static ru.kapodes.tictactoe.gamelogic.session.GameSession.create;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GameLogicTest {
    @Mock
    private PlayersService players;
    @Mock
    private SessionsService sessions;
    @Mock
    private ComputerStrategy strategy;
    @Mock
    private GameValidator validator;
    @Mock
    private TimeService times;
    @Mock
    private LeaderBoards leaders;

    private GameLogic logic;

    private Player player = new Player("kapodes");
    private Player computer = new Player("computer");
    private Game game = spy(newGame(player, computer));
    private GameSession session = create(game, player, computer);

    @Before
    public void setUp() {
        when(players.computer()).thenReturn(computer);
        when(sessions.session(session.getUuid(), player)).thenReturn(session);
        when(sessions.session(session.getUuid(), computer)).thenReturn(session);
        logic = new GameLogic(players, sessions, strategy, validator, times, leaders);
    }

    @Test
    public void should_create_new_game_with_computer_and_start_player_turn() {
        UUID expectedId = randomUUID();
        when(sessions.startNew(any(Game.class), same(player), same(computer))).thenReturn(expectedId);

        //when
        UUID resultId = logic.createNewWithComputer(player);

        //then
        assertEquals(expectedId, resultId);
        verify(times).startPlayerTurn(expectedId, player);
    }

    @Test(expected = BadRequestException.class)
    public void should_throw_bad_request_when_try_to_move_on_finished_session() {
        session.setGameResult(WON);
        when(sessions.session(session.getUuid(), player)).thenReturn(session);

        //when
        logic.makePlayerTurn(session.getUuid(), player, 0, 0);

        //then BadRequestException
    }

    @Test
    public void should_make_player_turn() {
        when(validator.validate(same(game), any(MatrixPosition.class))).thenReturn(DRAW);

        //when
        GameResult gameResult = logic.makePlayerTurn(session.getUuid(), player, 0, 0);

        //then
        assertEquals(DRAW, gameResult);
        assertEquals(DRAW, session.getGameResult());

        verify(game).makeAMove(player, 0, 0);
        verify(times).endPlayerTurn(session.getUuid(), player);
    }

    @Test
    public void should_make_computer_turn() {
        when(strategy.nextMove(session.getGame())).thenReturn(positionOf(1, 1));
        when(validator.validate(same(game), any(MatrixPosition.class))).thenReturn(PENDING);
        logic.makePlayerTurn(session.getUuid(), player, 0, 0);

        //when
        GameResult gameResult = logic.makeComputerTurn(session.getUuid());

        //then
        assertEquals(PENDING, gameResult);
        assertEquals(PENDING, session.getGameResult());

        verify(game).makeAMove(computer, 1, 1);
        verify(times).endPlayerTurn(session.getUuid(), computer);
    }

    @Test
    public void should_start_other_player_turn_if_game_pending() {
        when(validator.validate(same(game), any(MatrixPosition.class))).thenReturn(PENDING);

        //when
        logic.makePlayerTurn(session.getUuid(), player, 0, 0);

        //then
        verify(times).startPlayerTurn(session.getUuid(), session.otherPlayer(player));
    }

    @Test
    public void should_start_NOT_other_player_turn_and_collect_stats_if_game_won() {
        when(validator.validate(same(game), any(MatrixPosition.class))).thenReturn(WON);

        //when
        logic.makePlayerTurn(session.getUuid(), player, 0, 0);

        //then
        verify(leaders).collectStats(player, session, session.getGame().turnsCount(player));
        verify(times, times(0)).startPlayerTurn(session.getUuid(), session.otherPlayer(player));
    }
}