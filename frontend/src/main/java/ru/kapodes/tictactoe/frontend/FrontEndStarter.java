package ru.kapodes.tictactoe.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontEndStarter {

    public static void main(String[] args) {
        SpringApplication.run(FrontEndStarter.class, args);
    }
}
