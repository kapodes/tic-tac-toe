var playerTurn;
const prefix = "http://localhost:8090/gamelogic/";

$(document).ready(initGame);

function initGame() {
    $("div#board").hide();
    $("span#playing").hide();
    $("#leaders").click(leaders);
    $("#usersubmit").click(enterUser);
    $("#newGame").prop("disabled", true);
    $("#newGame").click(newGame);
    $("input:checkbox").hide();
}

function leaders() {
    $.ajax({
        type: 'GET',
        url: prefix + 'leaders',
        success: function (data) {
            console.log('leaders', data);
        }
    });
}

function enterUser() {
    var name = $("#username").val();
    var url = prefix + "players/" + name;
    $.ajax({
        type: 'POST',
        url: url,
        success: function (data) {
            console.log('success', data);
        }
    });

    $("span#playing").text("Now playing: " + name);
    $("span#playing").show();
    $("div#user").hide();
    $("#newGame").prop("disabled", false);
}

function newGame() {
    console.log("newGame");
    $("input:checkbox").each(initCheckbox);
    var name = $("#username").val();
    var url = prefix + "game/computer/?name=" + name;
    $.ajax({
        type: 'POST',
        url: url,
        success: function (gameId) {
            console.log('gameId', gameId);
            $("#gameId").text(gameId);
            getGame(gameId);
        }
    });
    playerTurn = true;
    $("div#board").show();
}

function initCheckbox(i, checkBox) {
    checkBox.indeterminate = true;
    checkBox.disabled = false;
    setLabelText(checkBox.id, " ");
    checkBox.onclick = checkClick;
}

function getGame(gameId) {
    var name = $("#username").val();
    var url = prefix + "game/" + gameId + "?name=" + name;
    $.ajax({
        type: 'GET',
        url: url,
        success: updateGame
    });
}

function updateGame(data) {
    $("#status").text(data.result);
    if (data.result !== "PENDING") {
        disableField();
    }
    $("#nextTurn").text(data.nextTurnPlayer.name);
    $(data.markedPositions).each(updateTile);
}

function updateTile(i, mark) {
    var id = "r" + mark.row + "c" + mark.column;
    setLabelText(id, mark.mark);
}

function setLabelText(id, text) {
    $("label[for=" + id + "]").text(text);
}

function checkClick() {
    if (!$(this).checked && playerTurn) {
        $(this).prop('disabled', true);
        var row = $(this).attr('data-r');
        var col = $(this).attr('data-c');
        makePlayerTurn(row, col);
        var gameId = $("#gameId").text();
        getGame(gameId);
        window.setTimeout(makeComputerTurn, 1000);
    }
}

function makePlayerTurn(row, col) {
    var gameId = $("#gameId").text();
    var name = $("#username").val();
    var url = prefix + "game/" + gameId + "/playerTurn/" + name + "/?row=" + row + "&column=" + col;

    $.ajax({
        type: 'POST',
        url: url,
        success: function (result) {
            console.log('result', result);
            getGame(gameId);
        }
    });

    playerTurn = !playerTurn;
}

function makeComputerTurn() {
    if ($("#status").text() !== "PENDING") return;
    var gameId = $("#gameId").text();
    var url = prefix + "game/" + gameId + "/computerTurn";
    $.ajax({
        type: 'POST',
        url: url,
        success: function (result) {
            console.log('result', result);
            getGame(gameId);
        }
    });

    playerTurn = !playerTurn;
}

function disableField() {
    $("input:checkbox").each(function (i, box) {
        box.disabled = true;
    });
}
